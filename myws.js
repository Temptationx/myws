const ws = require('ws')
const url = require('url')

var pathnames = {}

var upgradeFunction = function (request, socket, head) {
    const pathname = url.parse(request.url).pathname;
    if(pathname in pathnames){
        var wss = pathnames[pathname]
        wss.handleUpgrade(request, socket, head, ws=>{
            wss.emit('connection', ws)
        })
    }else{
        socket.destroy()
    }
}

module.exports = {
    Server: Server
}
var server_add = false
function Server(options){
    if(options.path && options.server){
        var wss = new ws.Server({noServer: true})
        pathnames[options.path] = wss
        if(!server_add){
            options.server.on('upgrade', upgradeFunction)
            server_add = true
        }
        return wss
    }
    else{
        return new ws.Server(options)
    }
}